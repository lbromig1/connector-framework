# Unobservable Properties

Add the `UnobservableProperty` decorator to a method to turn it into an unobservable property.

```python
@sila.UnobservableProperty()
async def get_random_number() -> int:
    """Returns a random number."""
    return 42
```

[Previous (Properties and Command)](../3.concepts/2.properties-and-commands.md) &nbsp; &nbsp; &nbsp; &nbsp;
[Next (Observable Property)](../3.concepts/4.observable-property.md)
